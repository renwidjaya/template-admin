<nav class="side-navbar">
    <div class="sidebar-header d-flex align-items-center">
        @php
            $default = 'https://icons-for-free.com/iconfiles/png/512/headset+male+man+support+user+young+icon-1320196267025138334.png';
            $photo = (!empty(Session::get('admin_photo'))) ? asset('images/users').'/'.Session::get('admin_photo') : $default;
        @endphp
        <div class="avatar"><img src="{{ $photo }}" alt="..." class="img-fluid rounded-circle"></div>
        <div class="title">
            <h1 class="h4">{{ Session::get('admin_name') }}</h1>
            <p>{{ Session::get('admin_title') }}</p>
        </div>
    </div>
    <span class="heading">Main</span>
    @php $dashboard = (strpos($_SERVER['REQUEST_URI'], 'dashboard') == true) ? 'active' : ''; @endphp
    <ul class="list-unstyled">
        <li class="{{ $dashboard }}">
            <a href="{{url('/dashboard')}}">
                <i class="fa fa-home"></i>
                Dashboard
            </a>
        </li>
        @foreach($menu as $parentMenu => $index)
            @if(!empty($index['child']))
                @php
                    $parentExpand = (strpos($_SERVER['REQUEST_URI'], Request::segment(1)) == true && Request::segment(1) !== 'dashboard') ? 'true' : '';
                    $parentActive = (strpos($_SERVER['REQUEST_URI'], Request::segment(1)) == true && Request::segment(1) !== 'dashboard') ? 'active' : '';
                    $parentShow   = (strpos($_SERVER['REQUEST_URI'], Request::segment(1)) == true && Request::segment(1) !== 'dashboard') ? 'show' : '';
                @endphp
                <li class="{{ $parentActive }}">
                    <a href="#dropdown{{ $parentMenu }}" aria-expanded="{{ $parentExpand }}" data-toggle="collapse">
                        <i class="demo-icon">{!! $index['icon']['menu_icon_unicode'] !!} </i>{{ $index['menu_parent_name'] }}
                    </a>
                    <ul id="dropdown{{ $parentMenu }}" class="collapse list-unstyled {{ $parentShow }}">
                        @foreach($index['child'] as $child)
                            @if($child['status'] == 1)
                                @php $activeList = (strpos($_SERVER['REQUEST_URI'], $child['menu_child_url']) == true) ? 'active' : ''; @endphp
                                <li class="{{ $activeList }}"><a href="{{ url($child['menu_child_url']) }}">{{ $child['menu_child_name'] }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            @endif
        @endforeach
    </ul>
</nav>
