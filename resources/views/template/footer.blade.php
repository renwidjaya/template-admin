<script src="{{url('/js/core/jquery.min.js')}}"></script>
<script src="{{url('/js/core/popper.min.js')}}"> </script>
<script src="{{url('/js/core/bootstrap.min.js')}}"></script>
<script src="{{url('/js/core/jquery.cookie.js')}}"> </script>
<script src="{{url('/js/core/front.js')}}"></script>
<script src="{{url('/js/core/index.js')}}"></script>
<script src="{{url('/js/page/general/search/search.js')}}"></script>
