<?php

namespace App\Http\Responses\Web\Admin\Master;

use App\Models\Admin\AdminMaster;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class AdminSaveResponse implements Responsable
{
    public function toResponse($request)
    {
        $image = "";
        if (!empty($request->admin_photo)) {
            $image = $this->decodeImage($request->admin_photo);
        }

        try {
            AdminMaster::create([
                'admin_name'        => $request->admin_name,
                'admin_title'       => $request->admin_title,
                'admin_description' => $request->admin_description,
                'admin_email'       => $request->admin_email,
                'admin_password'    => Hash::make($request->admin_password),
                'role_id'           => $request->role_id,
                'admin_photo'       => $image,
                'admin_token'       => '',
                'status'            => '1'
            ]);

            $data['code'] = 200;
            $data['message'] = 'Success';
        } catch (Exception $e) {
            $data['code'] = 500;
            $data['message'] = $e->getMessage();
        }
        return response()->json($data);
    }

    public function decodeImage($file)
    {
        preg_match("/data\:image\/(.*)\;base64/",$file, $extension);
        $image = str_replace('data:image/'.$extension[1].';base64,', '', $file);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.$extension[1];
        \File::put(storage_path(). '/' . $imageName, base64_decode($image));
        return $imageName;
    }
}
