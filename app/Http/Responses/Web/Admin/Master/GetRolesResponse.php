<?php

namespace App\Http\Responses\Web\Admin\Master;

use App\Models\Admin\AdminRole;
use Illuminate\Contracts\Support\Responsable;

class GetRolesResponse implements Responsable
{
    public function toResponse($request)
    {
        $data = AdminRole::where('status', '1')->get();
        try {
            if (!$data->isEmpty()) {
                return response()->json([
                    'code' => 200,
                    'data' => $data,
                ], 200);
            }else{
                return response()->json([
                    'code' => 204,
                    'data' => [],
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'data' => $e->getMessage(),
            ], 200);
        }
    }
}
