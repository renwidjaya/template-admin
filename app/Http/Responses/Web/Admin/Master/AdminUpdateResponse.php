<?php

namespace App\Http\Responses\Web\Admin\Master;

use App\Models\Admin\AdminMaster;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Support\Responsable;

class AdminUpdateResponse implements Responsable
{
    public function toResponse($request)
    {
        $image = Session::get('admin_photo');
        if (!empty($request->admin_photo)) {
            $image = $this->decodeImage($request->admin_photo);
        }

        try {
            AdminMaster::where('admin_id', $request->admin_id)
                ->update([
                    'admin_name'        => $request->admin_name,
                    'admin_title'       => $request->admin_title,
                    'admin_description' => $request->admin_description,
                    'admin_email'       => $request->admin_email,
                    'role_id'           => $request->role_id,
                    'admin_photo'       => $image,
                    'status'            => '1'
                ]);
            Session::forget(['admin_name', 'admin_title', 'admin_photo']);
            Session::put([
                'admin_name'    => $request->admin_name,
                'admin_title'   => $request->admin_title,
                'admin_photo'   => $image
            ]);
            $data['code'] = 200;
            $data['message'] = 'Success';
        } catch (Exception $e) {
            $data['code'] = 500;
            $data['message'] = $e->getMessage();
        }
        return response()->json($data);
    }

    public function decodeImage($file)
    {
        preg_match("/data\:image\/(.*)\;base64/",$file, $extension);
        $image = str_replace('data:image/'.$extension[1].';base64,', '', $file);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.$extension[1];
        \File::put(base_path('public/images/users/') . $imageName, base64_decode($image));
        return $imageName;
    }
}
