<?php

namespace App\Http\Responses\Web\Admin\Master;

use App\Models\Admin\AdminMaster;
use Illuminate\Contracts\Support\Responsable;

class AdminDeleteResponse implements Responsable
{
    public function toResponse($request)
    {
        try {
            AdminMaster::where('admin_id', $request->admin_id)->update([ 'status' => '0' ]);
            $data['code'] = 200;
            $data['message'] = 'Success';
        } catch (\Exception $e) {
            $data['code'] = 500;
            $data['message'] = $e->getMessage();
        }
        return response()->json($data, 200);
    }
}
