<?php

namespace App\Http\Responses\Web\Admin\Master;

use App\Models\Admin\AdminMaster;
use Illuminate\Contracts\Support\Responsable;

class AdminDeleteBulkResponse implements Responsable
{
    public function toResponse($request)
    {
        foreach($request->admin_id as $admin_id){
            AdminMaster::where('admin_id', $admin_id)->update([ 'status' => '0' ]);
        }
        $data['code'] = 200;
        $data['message'] = 'Success';
        return response()->json($data);
    }
}
