<?php

namespace App\Http\Controllers;

use \Firebase\JWT\JWT;
use App\Models\Menu\Config;
use Illuminate\Http\Request;
use App\Models\Menu\MenuParent;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use App\Models\Admin\AdminRolePermission;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct(Request $request)
    {
        $this->request = $request;
        $this->redirect = back()->with('error', 'You have not access for that module');
        $config = Config::where('cms_config_id', '1')->first();
        Session::put([
            'brand' => $config->cms_config_brand,
            'skin' => $config->cms_config_skin
        ]);

        $menu  = MenuParent::with('icon')
                            ->with('child')
                            ->where('status', '1')
                            ->get();

        View::share('menu', $menu);
    }

    public function authorize($param="")
    {
        $token = Session::get('admin_token');
        try {
            $role = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            $permissions = AdminRolePermission::join('menu_child', 'menu_child.menu_child_id', '=', 'admin_role_permission.menu_id')->where('role_id', $role->role_id)->get();
            foreach($permissions as $permission){
                if (!empty($param) && $param == 'view') {
                    if (strpos($permission['menu_child_url'], $this->request->path()) == true) {
                        $permissionAdd = $this->authView($permission);
                        if (!$permissionAdd) {
                            return $this->redirect;
                        }
                        Session::forget('error');
                        return $permissionAdd;
                    }
                }

                if (!empty($param) && $param == 'add') {
                    if ($permission['menu_child_url'] == $this->request->path()) {
                        $permissionAdd = $this->authAdd($permission);
                        if (!$permissionAdd) {
                            return $this->redirect;
                        }
                        Session::forget('error');
                        return $permissionAdd;
                    }
                }

                if (!empty($param) && $param == 'update') {
                    if ($permission['menu_child_url'] == $this->request->path()) {
                        $permissionAdd = $this->authUpdate($permission);
                        if (!$permissionAdd) {
                            return $this->redirect;
                        }
                        Session::forget('error');
                        return $permissionAdd;
                    }
                }

                if (!empty($param) && $param == 'delete') {
                    if ($permission['menu_child_url'] == $this->request->path()) {
                        $permissionAdd = $this->authDelete($permission);
                        if (!$permissionAdd) {
                            return $this->redirect;
                        }
                        Session::forget('error');
                        return $permissionAdd;
                    }
                }

                if (!empty($param) && $param == 'other') {
                    if ($permission['menu_child_url'] == $this->request->path()) {
                        $permissionAdd = $this->authOther($permission);
                        if (!$permissionAdd) {
                            return $this->redirect;
                        }
                        Session::forget('error');
                        return $permissionAdd;
                    }
                }
            }
            Session::forget('error');
            return true;
        } catch (\Exception $e) {
            return back();
        }
    }

    private function authView($permission)
    {
        return $result = ($permission['menu_view'] == '1') ? true : false;
    }

    private function authAdd($permission)
    {
        return $result = ($permission['menu_add'] == '1') ? true : false;
    }

    private function authUpdate($permission)
    {
        return $result = ($permission['menu_update'] == '1') ? true : false;
    }

    private function authDelete($permission)
    {
        return $result = ($permission['menu_delete'] == '1') ? true : false;
    }

    private function authOther($permission)
    {
        return $result = ($permission['menu_other'] == '1') ? true : false;
    }
}
