<?php

use Illuminate\Database\Seeder;

class MenuChildTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $child = [
            [
                'menu_child_name' => 'Admin Master',
                'menu_child_url' => 'admin-master',
                'menu_parent_id' => 1
            ],
            [
                'menu_child_name' => 'Admin Roles',
                'menu_child_url' => 'admin-roles',
                'menu_parent_id' => 1
            ],
            [
                'menu_child_name' => 'Admin Activity',
                'menu_child_url' => 'admin-activity',
                'menu_parent_id' => 1
            ]
        ];

        DB::table('menu_child')->insert($child);
    }
}
