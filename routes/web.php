<?php

$router->group(['namespace' => 'Main'], function () use ($router) {
    // auth route
    $router->group(['prefix' => '/'], function () use ($router) {
        $router->get('/', 'Auth\AuthController@index');
        $router->post('/login', 'Auth\AuthController@login');
        $router->get('/logout', 'Auth\AuthController@logout');
    });

    $router->group(['middleware' => 'auth'], function () use ($router) {

        // configuration side menu route
        $router->group(['prefix' => '/menu/config'], function () use ($router) {
            $router->get('/', 'Menu\ConfigController@index');
            $router->post('/update/{id}', 'Menu\ConfigController@update');
        });

        $router->group(['prefix' => '/menu/icon'], function () use ($router) {
            $router->get('/data', 'Menu\IconController@getData');
            $router->get('/', 'Menu\IconController@index');
            $router->get('/add', 'Menu\IconController@add');
            $router->post('/save', 'Menu\IconController@save');
            $router->get('/edit/{id}', 'Menu\IconController@edit');
            $router->post('/update/{id}', 'Menu\IconController@update');
            $router->get('/delete', 'Menu\IconController@delete');
            $router->post('/delete/many', 'Menu\IconController@bulkDelete');
        });

        $router->group(['prefix' => '/menu/parent'], function () use ($router) {
            $router->get('/data', 'Menu\ParentController@getData');
            $router->get('/', 'Menu\ParentController@index');
            $router->get('/add', 'Menu\ParentController@add');
            $router->post('/save', 'Menu\ParentController@save');
            $router->get('/edit/{id}', 'Menu\ParentController@edit');
            $router->post('/update/{id}', 'Menu\ParentController@update');
            $router->get('/delete', 'Menu\ParentController@delete');
            $router->post('/delete/many', 'Menu\ParentController@bulkDelete');
            $router->get('/get-icon', 'Menu\ParentController@getIcon');
        });

        $router->group(['prefix' => '/menu/child'], function () use ($router) {
            $router->get('/data', 'Menu\ChildController@getData');
            $router->get('/', 'Menu\ChildController@index');
            $router->get('/add', 'Menu\ChildController@add');
            $router->post('/save', 'Menu\ChildController@save');
            $router->get('/edit/{id}', 'Menu\ChildController@edit');
            $router->post('/update/{id}', 'Menu\ChildController@update');
            $router->get('/delete', 'Menu\ChildController@delete');
            $router->post('/delete/many', 'Menu\ChildController@bulkDelete');
            $router->get('/get-parent', 'Menu\ChildController@getParent');
        });


        // application route
        $router->group(['prefix' => '/dashboard'], function () use ($router) {
            $router->get('/', 'Dashboard\DashboardController@index');
        });

        $router->group(['prefix' => '/admin-master'], function () use ($router) {
            $router->get('/data', 'Admin\AdminController@getData');
            $router->get('/', 'Admin\AdminController@index');
            $router->get('/add', 'Admin\AdminController@add');
            $router->post('/save', 'Admin\AdminController@save');
            $router->get('/detail/{id}', 'Admin\AdminController@detail');
            $router->get('/edit/{id}', 'Admin\AdminController@edit');
            $router->post('/update/{id}', 'Admin\AdminController@update');
            $router->get('/delete', 'Admin\AdminController@delete');
            $router->post('/delete/many', 'Admin\AdminController@bulkDelete');
            $router->get('/get-roles', 'Admin\AdminController@getRoles');
        });

        $router->group(['prefix' => '/admin-roles'], function () use ($router) {
            // the role here
            $router->get('/data', 'Admin\RoleController@getData');
            $router->get('/', 'Admin\RoleController@index');
            $router->get('/add', 'Admin\RoleController@add');
            $router->post('/save', 'Admin\RoleController@save');
            $router->get('/edit/{id}', 'Admin\RoleController@edit');
            $router->post('/update/{id}', 'Admin\RoleController@update');
            $router->get('/delete', 'Admin\RoleController@delete');
            $router->post('/delete/many', 'Admin\RoleController@bulkDelete');

            // the permission
            $router->get('/get-permission', 'Admin\PermissionController@getData');
            $router->get('/permission/{id}', 'Admin\PermissionController@index');
            $router->get('/permission/{id}/save', 'Admin\PermissionController@save');
        });

    });
});
